#define SIEVE_COLLATZ_CUDA
static void HandleError( cudaError_t err,
						 const char *file,
						 int line ) {
	if (err != cudaSuccess) {
		printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
				file, line );
		exit( EXIT_FAILURE );
	}
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))
__device__ char source_brut(uint64_t b, uint64_t b0, uint64_t deltaN, int k, int c){
	uint64_t lenList, bm, cm;
	lenList = ((deltaN+1) < (b0-1)) ? (deltaN+1) : (b0-1) ;   // get min(deltaN+1, b0-1)
	for(uint64_t m=1; m<lenList; m++) {    // loop over starting numbers
		bm = b0-m;
		cm = 0;
		// take steps to update lists
		for(int j=0; j<k; j++) {
			if (bm & 1) {                 // bitwise test for odd
				bm += (bm >> 1)+1;    // note that bm is odd
				cm++;
			} else {
				bm >>= 1;
			}
		}
		// check against original path
		if ( bm==b && cm==c ) {
			return 1;
		}
	}
	return 0;
}
//https://stackoverflow.com/questions/5447570/cuda-atomic-operations-on-unsigned-chars
__device__ static inline unsigned char atomicXor2(unsigned char* address, unsigned char val) {
    size_t long_address_modulo = (size_t) address & 3;
    auto* base_address = (unsigned int*) ((unsigned char*) address - long_address_modulo);
    unsigned int long_val = (unsigned int) val << (8 * long_address_modulo);
    unsigned int long_old = atomicXor(base_address, long_val);

    if (long_address_modulo == 3) {
        // the first 8 bits of long_val represent the char value,
        // hence the first 8 bits of long_old represent its previous value.
        return (unsigned char) (long_old >> 24);
    } else {
        // bits that represent the char value within long_val
        unsigned int mask = 0x000000ff << (8 * long_address_modulo);
        unsigned int masked_old = long_old & mask;
        return (unsigned char) (masked_old >> 8 * long_address_modulo);
    }
}
const unsigned char sieve8[16] = {27,  31,  47,  71,  91, 103, 111, 127, 155, 159, 167, 191, 231, 239, 251, 255};
__global__ void find_forms(unsigned char* L, uint64_t len, uint64_t maximum, uint32_t* c, unsigned char* sieve8, unsigned char d, uint64_t ecart, uint64_t ecartp1){
	/*uint64_t* LI = (uint64_t*)malloc(sizeof(uint64_t)*d*2);
	unsigned char* L2 = (unsigned char*)malloc(sizeof(unsigned char)*d*2);
	unsigned char* L3 = (unsigned char*)malloc(sizeof(unsigned char)*d*2);*/
	uint64_t n = threadIdx.x+blockIdx.x*blockDim.x;
	uint64_t add = blockDim.x*gridDim.x;
	for(;n<maximum; n += add){
		uint64_t a=len;
		uint64_t b = ((((uint64_t)n)>>4)<<8)^sieve8[n&15];
		uint64_t ad=len, bd=b;
		unsigned char nodd=0;
		do{
			if(b&1){
				// *3+1)/2
				a += a >> 1;
				b += (b>>1)+1;
				nodd++;
			}
			while(!(b&1 || a&1)){
				// /2
				a >>= 1;
				b >>= 1;
			}
		}while((a&1)^1 && a > ad);
		if(a>ad){
			//if(!source(a, b, ad, bd, neven, d, LI, L2, L3)){
			if(!source_brut(b, bd, ecart, d, nodd)){
				uint64_t b1 = b, b2 = b+a;
				int nodd0=nodd, nodd1=nodd;
				if(b1&1){
					b1 += (b1>>1)+1;
					b2 >>= 1;
					nodd0++;
				}else{
					b2 += (b2>>1)+1;
					b1 >>= 1;
					nodd1++;
				}
				if( (b1 > bd     && !source_brut(b1, bd    , ecartp1, d+1, nodd0))||\
					(b2 > bd+len && !source_brut(b2, bd+len, ecartp1, d+1, nodd1))){
					switch(bd%9){
						case 0:
							atomicXor2(&L[n>>3            ], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*3)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*4)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*6)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*7)>>3], (unsigned char)(1<<(n&7)));
							break;
						case 1:
							atomicXor2(&L[n>>3            ], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*2)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*5)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*6)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*8)>>3], (unsigned char)(1<<(n&7)));
							break;
						case 2:
							atomicXor2(&L[(n+maximum  )>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*2)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*4)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*7)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*8)>>3], (unsigned char)(1<<(n&7)));
							break;
						case 3:
							atomicXor2(&L[n>>3            ], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum  )>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*3)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*4)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*6)>>3], (unsigned char)(1<<(n&7)));
							break;
						case 4:
							atomicXor2(&L[(n+maximum*2)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*3)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*5)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*6)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*8)>>3], (unsigned char)(1<<(n&7)));
							break;
						case 5:
							atomicXor2(&L[(n+maximum  )>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*4)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*5)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*7)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*8)>>3], (unsigned char)(1<<(n&7)));
							break;
						case 6:
							atomicXor2(&L[n>>3            ], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum  )>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*3)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*6)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*7)>>3], (unsigned char)(1<<(n&7)));
							break;
						case 7:
							atomicXor2(&L[n>>3            ], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*2)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*3)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*5)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*8)>>3], (unsigned char)(1<<(n&7)));
							break;
						case 8:
							atomicXor2(&L[(n+maximum  )>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*2)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*4)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*5)>>3], (unsigned char)(1<<(n&7)));
							atomicXor2(&L[(n+maximum*7)>>3], (unsigned char)(1<<(n&7)));
							break;
					}
					//atomicXor2(&L[n>>3], (unsigned char)(1<<(n&7)));
					atomicAdd(c, 5);
				}
			}
		}
	}
}
const uint64_t LenSieve[33] = {0, 0, 1, 2, 3, 4, 7, 11, 16, 31, 52, 103, 182, 297, 593, 1049, 1720, 3439, 6104, 12194, 22244, 38019, 75969, 137657, 234156, 467895, 847493, 1442349, 2882872, 5226985, 10446423, 19347686, 33880411};
int find_power(size_t space){
	for(int i=7; i<33; i++){
		int space_ = max((((size_t)1)<<i-7)*9, LenSieve[i]*sizeof(uint32_t)*5);
		if(space_ > space)return i-1;
	}
	return 32;
}
#define lb3 1.5849625007211563 //log2(3)
int sieve(uint32_t** forms, unsigned char* dev_sieve8, uint64_t* len, uint32_t* power, uint64_t* number){
	//determine le bon nombre de blocks en fonction du maximum
	//détermine le bon power en fonction de la mémoire
	size_t free, total;// initialisation
	CUdevice dev;
	CUcontext ctx;
	cuInit(0);
	cuDeviceGet(&dev, 0);
	cuCtxCreate(&ctx, 0, dev);// fin
	cuMemGetInfo(&free, &total);//informations sur la mémorie libre et total
	size_t len_max = free*90/100;//90/100 pour prendre une marge
	(*power) = (uint32_t)find_power(len_max);
	(*len) = ((uint64_t)1)<<(*power);
	uint32_t* dev_c;
	uint32_t c = 0;
	(*number) = (*len)*16/256;//len(sieve8)/2**8
	uint64_t maxi_len = ((*number)+7>>3)*9;//ceil(number/8*9)
	unsigned char* L = (unsigned char*)calloc(maxi_len, sizeof(char));
	printf("%llu\n", *number);
	uint64_t ecart   =1<<(((*power) -   ((uint64_t)( (*power)   /lb3+1.0)))*9/16);
	uint64_t ecartp1 =1<<(((*power)+1 - ((uint64_t)(((*power)+1)/lb3+1.0)))*9/16);
	printf("%d\n", ecart);
	unsigned char* dev_L;
	HANDLE_ERROR(cudaMalloc((void**)&dev_L, maxi_len));
	HANDLE_ERROR(cudaMalloc((void**)&dev_c, sizeof(uint32_t)));
	HANDLE_ERROR(cudaMemcpy(dev_c, &c, sizeof(uint32_t), cudaMemcpyHostToDevice));
	find_forms<<<512, 512>>>(dev_L, *len, *number, dev_c, dev_sieve8, *power, ecart, ecartp1);
	cudaDeviceSynchronize();
	printf("device %s\n", cudaGetErrorString( cudaGetLastError()));
	HANDLE_ERROR(cudaMemcpy( L, dev_L, maxi_len		   , cudaMemcpyDeviceToHost));
	HANDLE_ERROR(cudaMemcpy(&c, dev_c, sizeof(uint32_t), cudaMemcpyDeviceToHost));
	cudaFree(dev_L);
	cudaFree(dev_c);
	(*forms)=(uint32_t*)malloc(c*sizeof(uint32_t));
	printf("longueur des formes %lu\n", c);
	fprintf(stderr, "%f gagnées (%d/%llu)\n", (1-(double)c/((double)(*len)*9))*100, c, (*len)*9);
	//printf("%f gagnées\n", (1-(double)c/(double)len)*100);
	uint32_t d=0;
	for(uint32_t i=0; i<(*number)*9; i++){
		if(L[i>>3]&(1<<(i&7))){
			(*forms)[d] = i;
			if(d == c)break;
			d++;
		}
	}
	cudaFreeHost(L);
	return c;
}

SRCCU = $(wildcard *.cu)
all:$(SRCCU)

%.out: %.cu
	nvcc $< -o $@  -I /usr/local/cuda/include/  -L /usr/local/cuda/lib/ -lcuda
	

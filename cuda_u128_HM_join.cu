#include <time.h>
#include <cuda.h>
#include <stdio.h>
//#include <stdlib.h>
#include <inttypes.h>
#include "uint128_cuda.cu"
#include <errno.h>
#include "sieve.cu"
#define BILLION  1000000000L;
//https://dev.library.kiwix.org/content/stackoverflow_en_nopic_2021-08/questions/11656241/how-to-print-uint128-t-number-using-gcc

#define P10_UINT64 10000000000000000000ULL	 /* 19 zeroes */
#define E10_UINT64 19

#define STRINGIZER(x)	 # x
#define TO_STRING(x)	STRINGIZER(x)
//https://stackoverflow.com/questions/32530604/how-can-i-get-number-of-cores-in-cuda-device
int getSPcores(cudaDeviceProp devProp)
{  
	int cores = 0;
	int mp = devProp.multiProcessorCount;
	switch (devProp.major){
		case 2: // Fermi
			if (devProp.minor == 1) cores = mp * 48;
			else cores = mp * 32;
			break;
		case 3: // Kepler
			cores = mp * 192;
			break;
		case 5: // Maxwell
			cores = mp * 128;
			break;
		case 6: // Pascal
			if ((devProp.minor == 1) || (devProp.minor == 2)) cores = mp * 128;
			else if (devProp.minor == 0) cores = mp * 64;
			else printf("Unknown device type\n");
			break;
		case 7: // Volta and Turing
			if ((devProp.minor == 0) || (devProp.minor == 5)) cores = mp * 64;
			else printf("Unknown device type\n");
			break;
		case 8: // Ampere
			if (devProp.minor == 0) cores = mp * 64;
			else if (devProp.minor == 6) cores = mp * 128;
			else if (devProp.minor == 9) cores = mp * 128; // ada lovelace
			else printf("Unknown device type\n");
			break;
		case 9: // Hopper
			if (devProp.minor == 0) cores = mp * 128;
			else printf("Unknown device type\n");
			break;
		default:
			printf("Unknown device type\n"); 
			break;
	}
	return cores;
}
void print_time(double sec){
	if(sec>60){
		int m = sec/60;
		sec -= 60*m;
		if(m>60){
			int h=m/60;
			m %= 60;
			if(h>24){
				int j=h/24;
				h %= 24;
				if(j>365){
					int a=j/365;
					j %= 365;
					printf("%d ans ", a);
				}
				printf("%d j ", j);
			}
			printf("%d h ", h);
		}
		printf("%d m ", m);
	}
	printf("%lf sec", sec);
}
__device__ static int dev_print_u128_u(__uint128_t u128)
{
	int rc;
	if (u128 > UINT64_MAX){
		__uint128_t leading	= u128 / P10_UINT64;
		uint64_t trailing = u128 % P10_UINT64;
		rc = dev_print_u128_u(leading);
		rc += printf("%." TO_STRING(E10_UINT64) PRIu64, trailing);
	}else{
		uint64_t u64 = u128;
		rc = printf("%" PRIu64, u64);
	}
	return rc;
}
static int print_u128_u(__uint128_t u128)
{
	int rc;
	if (u128 > UINT64_MAX){
		__uint128_t leading	= u128 / P10_UINT64;
		uint64_t trailing = u128 % P10_UINT64;
		rc = print_u128_u(leading);
		rc += printf("%." TO_STRING(E10_UINT64) PRIu64, trailing);
	}else{
		uint64_t u64 = u128;
		rc = printf("%" PRIu64, u64);
	}
	return rc;
}



/*__device__ inline int ctz_u128 (uint128_t u) {
	if(u.lo){
		return __ctzll(u.lo);
	}else if(u.hi){
		return __ctzll(u.hi)+64;
	}else{
		return 128;
	}
}*/
#define LUT_SIZE32 21
#define LUT_SIZE64 41
#define LUT_SIZE128 81
__global__ void testnumber(uint64_t mini, uint32_t* forms, uint128_t* max_alt, uint32_t maximum, uint128_t* current, char* copy, unsigned char* dev_sieve8, uint64_t maxi, unsigned int d, uint64_t block){//, uint128_t* lut){
	__uint128_t _n = threadIdx.x+blockIdx.x*blockDim.x;
	__uint128_t add = blockDim.x*gridDim.x;
	if(_n == 100)printf("Hello\n");
	for(; _n < maximum; _n += add){/*/
	__uint128_t id = threadIdx.x+blockIdx.x*blockDim.x;
	__uint128_t _n = id*block;
	__uint128_t maxi_n = _n+block;
	for(; _n < maxi_n; _n++){//*/
		uint128_t n(0, mini+(((uint64_t)forms[_n])>>4<<8)^dev_sieve8[forms[_n]&15]);
		//if(n.lo%3 == 2)break;
		uint128_t* empl = max_alt+_n;
		uint128_t i(0, n.lo);
		i.odd2_apply();
		do{
			/*while((i.lo&67108863) == 67108863)
				i.odd26_apply();*/
			while((i.lo&15) == 15)
				i.odd4_apply();
			if((i.lo&3)==3)
				i.odd2_apply();
			if(i.lo&1)
				i.odd_apply();
			i.setmaxin(empl);
			i >>= __ffsll(i.lo)-1;
			if(n.bigger(i))break;
		}while(1);
		if(empl->bigger(*current)){
			*copy = 1;
		}
	}
}
#define lb3 1.5849625007211563 //log2(3)
int main(){
	//determine le bon nombre de blocks en fonction du maximum
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	printf("début: %02d/%02d/%d %02d:%02d:%02d\n", tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
    cudaDeviceProp devProp;
    cudaGetDeviceProperties(&devProp, 0);
    unsigned int maxThreadPerBlocks = devProp.maxThreadsPerBlock;
	unsigned char* dev_sieve8;
	HANDLE_ERROR(cudaMalloc((void**)&dev_sieve8, 16));
	HANDLE_ERROR(cudaMemcpy(dev_sieve8, &sieve8, 16, cudaMemcpyHostToDevice));
	uint32_t* forms;
	uint64_t len, number;
	uint32_t power;
	uint32_t c = sieve(&forms, dev_sieve8, &len, &power, &number);
	printf("top\n");
	//initialisation dans host et dans device
	uint32_t* dev_forms;
	uint128_t* dev_maxalt;
	uint128_t* maxalt = (uint128_t*)calloc(c, sizeof(uint128_t));
	HANDLE_ERROR(cudaMalloc((void**)&dev_forms , c*sizeof(uint32_t)));
	HANDLE_ERROR(cudaMalloc((void**)&dev_maxalt, c*sizeof(uint128_t)));
	HANDLE_ERROR(cudaMemcpy(dev_forms, forms, c*sizeof(uint32_t), cudaMemcpyHostToDevice));
	uint64_t mini = 0;
	uint32_t forms_len = c;
	uint64_t len9 = 9*len;
	uint64_t maxi = len9;
	uint128_t maxialtall(0, 0);
	char* dev_copy;
	char copy=0;
	const char null_cpy = 0;
	uint128_t* current;
	HANDLE_ERROR(cudaMalloc((void**)&dev_copy, sizeof(char)));
	HANDLE_ERROR(cudaMalloc((void**)&current, sizeof(uint128_t)));
	HANDLE_ERROR(cudaMemcpy(current, &maxialtall, sizeof(uint128_t), cudaMemcpyHostToDevice));
	// fin; détermine le bombre de thread et de blocks
	uint32_t X=maxThreadPerBlocks;
	uint32_t Y=min((forms_len+X-1)/X, getSPcores(devProp));
	const uint64_t block = forms_len/(X*Y);
	
	//fin; pour la mesure du temps
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start);
	float accum=0;
	t = time(NULL);
	tm = *localtime(&t);
	printf("commence les calculs: %02d/%02d/%d %02d:%02d:%02d\n", tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
	//fin; commence à calculer
	while(mini < maxi){
		testnumber<<<Y, X>>>(mini, dev_forms, dev_maxalt, forms_len, current, dev_copy, dev_sieve8, number-1, power-4, block);//, dev_lut);
		cudaDeviceSynchronize();
		//printf("device %s\n", cudaGetErrorString( cudaGetLastError()));
		HANDLE_ERROR(cudaMemcpy(&copy, dev_copy, sizeof(char), cudaMemcpyDeviceToHost));
		if(copy){
			HANDLE_ERROR(cudaMemcpy(maxalt, dev_maxalt, forms_len*sizeof(uint128_t), cudaMemcpyDeviceToHost));
			for(uint64_t i=0; i<forms_len; i++){
				if(maxalt[i].bigger(maxialtall)){
					maxialtall.copy(maxalt[i]);
					print_u128_u((((__uint128_t)maxialtall.hi) << 64)^maxialtall.lo);
					uint64_t nf = mini+((((uint64_t)forms[i])>>4<<8)^sieve8[forms[i]&15]);
					printf(" avec n = %llu", nf);
					cudaEventRecord(stop);
					cudaEventSynchronize(stop);
					cudaEventElapsedTime(&accum, start, stop);
					printf(" en %f sec (", accum/1000);
					print_time(accum/1000);
					printf(")\n");
				}
			}
			HANDLE_ERROR(cudaMemcpy(current, &maxialtall, sizeof(uint128_t), cudaMemcpyHostToDevice));
			HANDLE_ERROR(cudaMemcpy(dev_copy, &null_cpy, sizeof(char), cudaMemcpyHostToDevice));
			fflush(stdout);
		}
		if(maxi>>(ffsll(maxi)-1) == 9){
			printf("9*2^%d en ", ffsll(maxi)-1);
			cudaEventRecord(stop);
			cudaEventSynchronize(stop);
			cudaEventElapsedTime(&accum, start, stop);
			print_time(accum/1000);
			printf("\n");
		}else if(ffsll(maxi)>60){
			printf("%d*2^%d en ", maxi>>ffsll(maxi)-1, ffsll(maxi)-1);
			cudaEventRecord(stop);
			cudaEventSynchronize(stop);
			cudaEventElapsedTime(&accum, start, stop);
			print_time(accum/1000);
			printf("\n");
		}
		mini += len9;
		maxi += len9;
	}
	cudaFreeHost(forms);
	cudaFree(dev_forms);
}

#include <inttypes.h>
#define T63 ((uint64_t)1)<<63
#define T62 ((uint64_t)1)<<62
class uint128_t{
public:
	uint64_t lo;
	uint64_t hi;
	__device__ __host__ uint128_t(){};
	__device__ __host__ uint128_t(uint64_t a, uint64_t b): hi(a), lo(b) {}
	template<typename T>
	__host__ __device__ uint128_t(const T & a){
		this->lo = (uint64_t) a & (uint64_t)-1;
		this->hi = 0;
	}
	__device__ __host__  void copy(uint128_t res){
		lo = res.lo, hi = res.hi;
	}
	__device__ void lshift1(void){
		hi = (hi<<1)|(lo>>63);
		/*if(lo>>63){
			hi |= 1;
		}*/
		lo <<= 1;
	}
	__device__ void rshift1(void){
		//uint64_t T63 = ;
		lo >>= 1;
		if(hi&1){
			lo |= T63;
		}
		hi >>= 1;
	}
	//https://github.com/curtisseizert/CUDA-uint128/blob/master/cuda_uint128.h
	__host__ __device__ void add(uint64_t hi2, uint64_t lo2){
		asm(  "add.cc.u64    %0, %2, %4;\n\t"
			  "addc.u64      %1, %3, %5;\n\t"
			  : "=l" (lo), "=l" (hi)
			  : "l" (lo), "l" (hi),
			    "l" (lo2), "l" (hi2));
	}
	__host__ __device__ void sub(uint64_t hi2, uint64_t lo2){
		asm(  "sub.cc.u64    %0, %2, %4;\n\t"
			  "subc.u64      %1, %3, %5;\n\t"
			  : "=l" (lo), "=l" (hi)
			  : "l" (lo), "l" (hi),
			    "l" (lo2), "l" (hi2));/*/
		if(lo < lo2) hi--;
		lo -= lo2;
		hi -= hi2;//*/
	}
	__host__ __device__ void add64b(int64_t n){
		asm(  "add.cc.u64    %0, %2, %4;\n\t"
			  "addc.u64      %1, %3, 0;\n\t"
			  : "=l" (lo), "=l" (hi)
			  : "l" (lo), "l" (hi)
				"l" (n));
	}
	__host__ __device__ inline void operator>>=(const int& b)
	{
	//    if (b == 0) {
	//      return *this;
	//    } else if (b < 64) {
		lo = (lo >> b) | (hi << (64-b));
		hi >>= b;
	//    } else {        // note that b >= 128 is undefined
	//      lo = hi >> (b-64);
	//      hi = 0;
	//    }
	}
	__device__ void add1(void){
		add64b(1);
	}
	__device__ void add2(void){
		add64b(2);
	}
	__device__ void odd_apply(void){
		//uint64_t hi2=hi, lo2=lo;
		add(hi>>1, ((lo>>1)|(hi<<63))|1);
		/* Ce code est exécuté que quand lo&3 == 1
		 * ce qui veut dire que lo finit (en binaire) par 01
		 * donc lo >> 1 finit par 0 (en binaire)
		 * donc à la place du +1 on peut faire le |1
		 */
	}
	__device__ void odd2_apply(void){
		uint64_t hi2=hi<<1|(lo>>63), lo2=lo<<1;
		lo = ((lo >> 2) | (hi << 62))+2;
		hi >>= 2;
		add(hi2, lo2);
	}
	__device__ void odd3_apply(void){
		add((hi<<1)|(lo>>63), lo<<1);
		add(hi>>3, ((lo>>3)|(hi<<61))+3);
	}
	__device__ void odd4_apply(void){
		uint64_t hi2 = hi>>4, lo2 = (hi<<60)|(lo>>4);
		add(hi<<2|(lo>>62), (lo<<2));
		add(hi2, lo2+5);
	}
	__device__ void odd12_apply(void){
		uint64_t hi1 = hi<<7|(lo>>57), lo1=lo<<7;
		uint64_t hi2 = hi>>1, lo2 = (hi<<63)|(lo>>1);
		uint64_t hi3 = hi>>2, lo3 = (hi<<62)|(lo>>2);
		uint64_t hi4 = hi>>8, lo4 = (hi<<56)|(lo>>8);
		uint64_t hi5 = hi>>12, lo5 = (hi<<52)|(lo>>12);
		add(hi1, lo1);
		add(hi2, lo2);
		add(hi3, lo3);
		sub(hi4, lo4);
		add(hi5, lo5);
		add64b(130);
	}
	__device__ void odd26_apply(){
		uint64_t hi1  = hi<<15|(lo>>49), lo1=lo<<15;
		uint64_t hi2  = hi<<12|(lo>>52), lo2=lo<<12;
		uint64_t hi3  = hi<<10|(lo>>54), lo3=lo<<10;
		uint64_t hi4  = hi<<4 |(lo>>60), lo4=lo<<4 ;
		uint64_t hi5  = hi<<2 |(lo>>62), lo5=lo<<2 ;
		uint64_t hi6  = hi>>1 , lo6  = (hi<<63)|(lo>>1 );
		uint64_t hi7  = hi>>2 , lo7  = (hi<<62)|(lo>>2 );
		uint64_t hi8  = hi>>9 , lo8  = (hi<<55)|(lo>>9 );
		uint64_t hi9  = hi>>11, lo9  = (hi<<53)|(lo>>11);
		uint64_t hi10 = hi>>21, lo10 = (hi<<43)|(lo>>21);
		uint64_t hi11 = hi>>23, lo11 = (hi<<41)|(lo>>23);
		lo = (hi<<38)|(lo>>26);
		hi >>= 26;
		add(hi1, lo1);
		add(hi2, lo2);
		add(hi3, lo3);
		sub(hi4, lo4);
		add(hi5, lo5);
		add(hi6, lo6);
		add(hi7, lo7);
		add(hi8, lo8);
		add(hi9, lo9);
		sub(hi10, lo10);
		add(hi11, lo11);
		add64b(37881);
	}
	__device__ char is_even(void){
		return (lo&1)^1;
	}
	__device__ char is_4k3(void){
		return (lo&3) == 3;
	}
    __device__ inline uint128_t & operator*=(const uint64_t & b){
		hi *= b;
		hi += __umul64hi(lo, b);
		lo *= b;
	}
	__device__ int ctz(void){
		if(lo){
			return __ffsll(lo)-1;
		}else
			return __ffsll(hi)+63;
	}
	__device__ static inline uint128_t mul128(uint128_t x, uint64_t y){
		uint128_t res;
		res.lo = x.lo * y;
		res.hi = __umul64hi(x.lo, y);
		res.hi += x.hi * y;
		return res;
	}

	__device__ char is_8k7(void){
		return (lo&7) == 7;
	}
	__device__ char is_odd(void){
		return lo&1;
	}
	__host__ __device__ char bigger(uint128_t b){
		if(hi<b.hi)return 0;
		else if(hi>b.hi)return 1;
		else if(lo>b.lo)return 1;
		else return 0;
	}
	__host__ __device__ char lower(uint128_t b){
		if(hi < b.hi)return 1;
		else if(hi == b.hi && lo < b.lo)return 1;
		else return 0;
	}
	__host__ __device__ char eqaul(uint128_t b){
		if(hi == b.hi && lo == b.lo)return 1;
		else return 0;
	}
	__host__ __device__ char cmp(uint128_t b){
		/* for a.cmp(b) :
		 * if a >  b: return 1
		 * if a <  b: return -1
		 * if a == b: return 0
		 */
		if(hi > b.hi)return 1;
		else if(hi < b.hi)return -1;
		else{
			if(lo > b.lo)return 1;
			else if(lo < b.lo)return -1;	
			else return 0;
		}
	}
	__device__ void lshift1_stock(uint128_t* res){
		res->hi = hi<<1;
		if(lo>>63){
			res->hi |= 1;
		}
		res->lo = lo << 1;
	}
	__device__ __host__ void xor_(uint128_t b){
		lo ^= b.lo, hi ^= b.hi;
	}
	__device__ void setmaxin(uint128_t* res){
		if(hi > res->hi){
			res->hi = hi;
			res->lo = lo;
		}else if(hi == res->hi){
			if(lo > res->lo)
				res->lo = lo;
		}
	}
	__device__ friend uint128_t operator*(uint128_t a, const uint64_t & b){return mul128(a, (uint64_t)b);}
};


#include <time.h>
#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "uint128_cuda.cu"
static void HandleError( cudaError_t err,
						 const char *file,
						 int line ) {
	if (err != cudaSuccess) {
		printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
				file, line );
		exit( EXIT_FAILURE );
	}
}
#define BILLION  1000000000L;
//https://dev.library.kiwix.org/content/stackoverflow_en_nopic_2021-08/questions/11656241/how-to-print-uint128-t-number-using-gcc
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

#define P10_UINT64 10000000000000000000ULL	 /* 19 zeroes */
#define E10_UINT64 19

#define STRINGIZER(x)	 # x
#define TO_STRING(x)	STRINGIZER(x)

__device__ static int dev_print_u128_u(__uint128_t u128)
{
	int rc;
	if (u128 > UINT64_MAX){
		__uint128_t leading	= u128 / P10_UINT64;
		uint64_t trailing = u128 % P10_UINT64;
		rc = dev_print_u128_u(leading);
		rc += printf("%." TO_STRING(E10_UINT64) PRIu64, trailing);
	}else{
		uint64_t u64 = u128;
		rc = printf("%" PRIu64"\n", u64);
	}
	printf("\n");
	return rc;
}
static int print_u128_u(__uint128_t u128)
{
	int rc;
	if (u128 > UINT64_MAX){
		__uint128_t leading	= u128 / P10_UINT64;
		uint64_t trailing = u128 % P10_UINT64;
		rc = print_u128_u(leading);
		rc += printf("%." TO_STRING(E10_UINT64) PRIu64, trailing);
	}else{
		uint64_t u64 = u128;
		rc = printf("%" PRIu64, u64);
	}
	return rc;
}



/*__device__ inline int ctz_u128 (uint128_t u) {
	if(u.lo){
		return __ctzll(u.lo);
	}else if(u.hi){
		return __ctzll(u.hi)+64;
	}else{
		return 128;
	}
}*/

const unsigned char sieve8[16] = {27,  31,  47,  71,  91, 103, 111, 127, 155, 159, 167, 191, 231, 239, 251, 255};
__global__ void find_forms(char* L, uint64_t len, uint64_t maximum, uint32_t* c, unsigned char* sieve8){
	__uint128_t n = threadIdx.x+blockIdx.x*blockDim.x;
	__uint128_t add = blockDim.x*gridDim.x;
	for(;n<maximum; n += add){
		__uint128_t a=len;
		__uint128_t b = (n/16)*256^sieve8[n%16];
		uint64_t ad=len;//, bd=b;
		while((!(a&1)) && a >= ad){
			if(b&1){
				// *3+1
				a *= 3;
				b = b*3+1;
			}
			while(!(b&1 || a&1)){
				// /2
				a >>= 1;
				b >>= 1;
			}
		}
		if(a>ad){
			L[n] = 1;
			atomicAdd(c, 1);
		}else{
			L[n] = 0;
		}
	}
}
__global__ void testnumber(uint64_t mini, uint64_t maxi, uint32_t* forms, uint128_t* max_alt, uint32_t maximum, uint128_t* current, char* copy){
	__uint128_t _n = threadIdx.x+blockIdx.x*blockDim.x;
	__uint128_t add = blockDim.x*gridDim.x;
	//if(_n == 100)printf("Hello\n");
	for(; _n < maximum; _n += add){
		uint128_t* empl = max_alt+_n;
		uint128_t n(0, mini^forms[_n]);
		if(n.lo%3 == 2)break;
		uint128_t i(0, n.lo);
		i.odd_apply();
		while (1){
			while(i.is_4k3())
				i.odd2_apply();
			if(i.is_odd())
				i.odd_apply();
			i.set2maxin(empl);
			i >>= __ffs(i.lo)-1;/*
			do{
				i.rshift1();
			}while(i.is_even());//*/
			if(n.bigger(i))break;
		}
		if(empl->bigger(*current)){
			*copy = 1;
		}
	}
}

int main(){
	//determine le bon nombre de blocks en fonction du maximum
    cudaDeviceProp devProp;
    cudaGetDeviceProperties(&devProp, 0);
    unsigned int maxThreadPerBlocks = devProp.maxThreadsPerBlock;
	//détermine le bon power en fonction de la mémoire
	size_t free, total;// initialisation
	CUdevice dev;
	CUcontext ctx;
	cuInit(0);
	cuDeviceGet(&dev, 0);
	cuCtxCreate(&ctx, 0, dev);// fin
	cuMemGetInfo(&free, &total);//informations sur la mémorie libre et total
	size_t len_max = free*90/100*10/(9*4+10) *256/16;//90/100 pour prendre une marge, *256/16
	size_t n = len_max;
	printf("%llu\n", free);
	int i=0;
	while (n){
		i ++;
		n>>= 1;
	}//i = |log2(n)|
	i = (i|1)-1; // le rend paire, car souvent le % gagné entre 2**(2k) et 2**(2k+1) est égale
	if(i > 32){
		size_t len_max2 = free*90/100*10/(9*8+10) *256/16;
		size_t n = len_max2;
		int i=0;
		while (n){
			i ++;
			n>>= 1;
		}
		i = (i|1) -1;
		if(i > 32)printf("nous vous conseillons d'utiliser la version pour 64 bits : %d\n", i);
		i = 32;
	}
	i = min(i, 32);
	char* dev_L;
	unsigned char* dev_sieve8;
	HANDLE_ERROR(cudaMalloc((void**)&dev_sieve8, 16));
	HANDLE_ERROR(cudaMemcpy(dev_sieve8, &sieve8, 16, cudaMemcpyHostToDevice));
	uint32_t power = (uint32_t)i;
	printf("power = %d\n", i);
	uint64_t len = ((uint64_t)1)<<power;
	uint32_t* dev_c;
	uint32_t c = 0;
	size_t number = len*16/256;
	char* L = (char*)calloc(number, sizeof(char));
	printf("%llu\n", number);
	HANDLE_ERROR(cudaMalloc((void**)&dev_L, number));
	HANDLE_ERROR(cudaMalloc((void**)&dev_c, sizeof(uint32_t)));
	HANDLE_ERROR(cudaMemcpy(dev_c, &c, sizeof(uint32_t), cudaMemcpyHostToDevice));
	find_forms<<<maxThreadPerBlocks, maxThreadPerBlocks>>>(dev_L, len, number, dev_c, dev_sieve8);
	cudaDeviceSynchronize();
	printf("device %s\n", cudaGetErrorString( cudaGetLastError()));
	HANDLE_ERROR(cudaMemcpy( L, dev_L, number		   , cudaMemcpyDeviceToHost));
	HANDLE_ERROR(cudaMemcpy(&c, dev_c, sizeof(uint32_t), cudaMemcpyDeviceToHost));
	uint32_t* forms=(uint32_t*)malloc(c*sizeof(uint32_t));
	printf("longueur des formes %lu\n", c);
	printf("%f gagnées\n", (1-(double)c/(double)len)*100);
	uint32_t d=0;
	for(uint32_t i=0; i<number; i++){
		if(L[i]){
			forms[d] = (i/16)*256^sieve8[i%16];
			if(d == c)break;
			d++;
		}
	}
	cudaFree(dev_L);
	cudaFree(dev_c);
	cudaFreeHost(L);
	printf("top\n");
	//initialisation dans host et dans device
	uint32_t* dev_forms;
	uint128_t* dev_maxalt;
	uint128_t* maxalt = (uint128_t*)calloc(c, sizeof(uint128_t));
	HANDLE_ERROR(cudaMalloc((void**)&dev_forms , c*sizeof(uint32_t)));
	HANDLE_ERROR(cudaMalloc((void**)&dev_maxalt, c*sizeof(uint128_t)));
	HANDLE_ERROR(cudaMemcpy(dev_forms, forms, c*sizeof(uint32_t), cudaMemcpyHostToDevice));
	uint64_t mini = 0;
	uint32_t forms_len = c;
	uint64_t maxi = len;
	uint128_t maxialtall(0, 0);
	char* dev_copy;
	char copy=0;
	const char null_cpy = 0;
	uint128_t* current;
	HANDLE_ERROR(cudaMalloc((void**)&dev_copy, sizeof(char)));
	HANDLE_ERROR(cudaMalloc((void**)&current, sizeof(uint128_t)));
	HANDLE_ERROR(cudaMemcpy(current, &maxialtall, sizeof(uint128_t), cudaMemcpyHostToDevice));
	// fin; détermine le bombre de thread et de blocks
	uint32_t X=maxThreadPerBlocks;
	uint32_t Y=(forms_len+X-1)/X;
	
	//fin; pour la mesure du temps
	cudaEvent_t start, stop;//, startcpy, stopcpy;
	//cudaEventCreate(&startcpy);
	//cudaEventCreate(&stopcpy);
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start);
	float accum=0;
	//fin; commence à calculer
	while(mini < maxi){
		testnumber<<<Y, X>>>(mini, maxi, dev_forms, dev_maxalt, forms_len, current, dev_copy);
		cudaDeviceSynchronize();
		HANDLE_ERROR(cudaMemcpy(&copy, dev_copy, sizeof(char), cudaMemcpyDeviceToHost));
		//cudaEventRecord(startcpy);
		/*cudaEventRecord(stopcpy);
		cudaEventSynchronize(stopcpy);
		cudaEventElapsedTime(&accum, startcpy, stopcpy);
		printf("%f sec en copiant\n", accum/1000);*/
		if(copy){
			HANDLE_ERROR(cudaMemcpy(maxalt, dev_maxalt, forms_len*sizeof(uint128_t), cudaMemcpyDeviceToHost));
			for(uint64_t i=0; i<forms_len; i++){
				if(maxalt[i].bigger(maxialtall)){
					maxialtall.copy(maxalt[i]);
					print_u128_u((((__uint128_t)maxialtall.hi) << 64)^maxialtall.lo);
					printf(" avec n = %llu, m=%llu", forms[i]^mini, forms[i]);
					cudaEventRecord(stop);
					cudaEventSynchronize(stop);
					cudaEventElapsedTime(&accum, start, stop);
					printf(" en %f sec\n", accum/1000);
				}
			}
			HANDLE_ERROR(cudaMemcpy(current, &maxialtall, sizeof(uint128_t), cudaMemcpyHostToDevice));
			HANDLE_ERROR(cudaMemcpy(dev_copy, &null_cpy, sizeof(char), cudaMemcpyHostToDevice));
			fflush(stdout);
		}
		mini += len;
		maxi += len;
	}
	cudaFreeHost(forms);
	cudaFree(dev_forms);
}

# conjecture de collatz

programme à compiler | commande | utilisation | librairies utilisées | alertes
--- | --- | --- | --- | ---
cuda_version.cu | nvcc cuda_version.cu -I /usr/local/cuda/include/  -L /usr/local/cuda/lib/ -lcuda -o cuda -O3 | ./cuda | aucune | pas encore testé
cuda_version_u128_HandMake.cu | nvcc cuda_version_u128_HandMake.cu -I /usr/local/cuda/include/  -L /usr/local/cuda/lib/ -lcuda -o cuda_u128_HM -O3 | ./cuda_u128_HM | CUDA(language) | le 3ème plus rapide
cuda_u128_HM_join.cu | nvcc cuda_u128_HM_join.cu -I /usr/local/cuda/include/  -L /usr/local/cuda/lib/ -lcuda -o cuda_u128_HM_join -O3 | ./cuda_u128_HM_join | CUDA(language) | le plus rapide
cuda_u128_HM_join_pp.cu | nvcc cuda_u128_HM_join_pp.cu -I /usr/local/cuda/include/  -L /usr/local/cuda/lib/ -lcuda -o cuda_u128_HM_join_pp -O3 | ./cuda_u128_HM_join_pp | CUDA(language) | le 2ème plus rapide
c_version.c | gcc c_version.c -o c_version -lgmp -O3 | ./c_version | gmp | lent car utilise gmp
c_vesion_parallel.c | gcc c_version_parallel.c -o c_version_parallel -lgmp -fopenmp -O3 | ./c_version_parallel | gmp, omp | pas beaucoup plus rapide que c_version.c

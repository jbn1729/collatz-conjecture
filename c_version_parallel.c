#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include <unistd.h>
#include <time.h> 


unsigned long long add(unsigned long long** R, unsigned long long el, unsigned long long len){
	if(len){
		unsigned long long* Ri = calloc(len, sizeof(unsigned long long));
		for(int i=0; i<len; i++)Ri[i] = (*R)[i];
		(*R) = reallocarray((void*)(*R), (size_t)(len+1), sizeof(unsigned long long));
		if(*R == NULL){
			printf("error\n");
			exit(1);
		}
		for(int i=0; i<len; i++)(*R)[i]=Ri[i];
		(*R)[len] = el;
		free(Ri);
		return len+1;
	}else{
		if(((*R)=reallocarray(NULL, 1, sizeof(unsigned long long))) == NULL){
			printf("error\n");
			exit(1);
		}
		(*R)[0] = el;
		return 1;
	}
}

void ull2mpz(mpz_t n, unsigned long long ull)
{
    mpz_init_set_ui(n, (unsigned int)(ull >> 32)); /* n = (unsigned int)(ull >> 32) */
    mpz_mul_2exp(n, n, 32);                   /* n <<= 32 */
    mpz_add_ui(n, n, (unsigned int)ull);      /* n += (unsigned int)ull */
}

void ulls_to_mpzs(mpz_t* rop, unsigned long long* L, unsigned long long len){
	#pragma omp parallel for
	for(int i=0; i<len; i++){
		ull2mpz(rop[i], L[i]);
	}
}

void all_ecarts(unsigned long long* R, unsigned long long* W, unsigned long long lenW, unsigned long long thE){
	#pragma omp parallel for
	for(int i=1; i<lenW; i++){
		R[i-1] = W[i]-W[i-1];
	}
	R[lenW-1] = W[0]+thE-W[lenW-1];
}
char syr_of_form(unsigned long long a, unsigned long long b){
	/*retourne si la forme F passe sous lui-même avec l'application de syracuse :
	>>> syr_of_form([4, 1])
	True
	car 4 k+1 est impaire, donne :
		12k+4 qui est paire donc donne:
		6 k+2 qui est paire donc donne:
		3 k+1 3k+1 < 4k+1, donc le vol en altitude est fini*/
	unsigned long long ad=a, bd=b;
	while((!(a&1)) && a >= ad){
		if(b&1){
			// *3+1
			a *= 3;
			b = b*3+1;
		}
		while(!(b&1 || a&1)){
			// /2
			a >>= 1;
			b >>= 1;
		}
	}
	return a<ad;
}

unsigned int test_all_form_of_nk(unsigned long long n, unsigned long long** L){
	//retourne toutes les formes où syr_of_form retourne False où le multiple de k est égal au nombre fourni
	unsigned int lenL= 0;
	for(unsigned long long p=3; p<n; p+=4){
		if(!syr_of_form(n, p))
			lenL = add(L, p, lenL);
		if(p%10000 == 3)
			printf("%llu %u\n", p, lenL);
	}
	return lenL;
}


void apply(mpz_t i, mpz_t n){
	//gmp_printf("%Zd %Zd\n", i, n);
	mpz_mul_ui(i, n, 3);
	mpz_add_ui(i, i, 1);
	mpz_div_2exp(i, i, 1);
}

void test_unique_n(mpz_t n, mpz_t* max_alt){
	mpz_t i, i2, un;
	mpz_init(i);
	mpz_init(i2);
	mpz_init_set_ui(un, 1);
	apply(i, n);
	while (1){
		apply(i, i);
		mpz_and(i2, i, un);
		if(mpz_cmp_ui(i2, 0) == 0){
			mpz_mul_2exp(i2, i, 1);
			if(mpz_cmp(i2, *max_alt)>0){
				mpz_set(*max_alt, i2);
			}
			mpz_div_2exp(i, i, mpz_scan1(i, 0));
			if(mpz_cmp(i, n) < 0)break;
		}
	}
	mpz_clear(un);
	mpz_clear(i);
	mpz_clear(i2);
}

void parallel(unsigned long long forms_len, mpz_t* forms, unsigned long long first, unsigned long long lenpara){
	clock_t start = clock();
	mpz_t max_alt, n_max, mini;
	mpz_init_set_ui(max_alt, 0);
	mpz_init_set_ui(n_max, 0);
	// on transforme mini pour le rendre sous une bonne forme
	// car min est toujours divisible par 65536
	mpz_t n;
	//mpz_init(n);
	mpz_init_set_ui(n, 0);
	int t;
	mpz_t maxi, len_e;
	ull2mpz(len_e, lenpara);
	mpz_init_set(maxi, len_e);
	mpz_t* maxis=calloc(forms_len, sizeof(mpz_t));
	#pragma omp parallel for
	for(unsigned long long i=0; i<forms_len; i++)
		mpz_init(maxis[i]);
	while(1){
		#pragma omp parallel for
		for(unsigned int i=0; i<forms_len; i++){
			mpz_t inter2;
			mpz_init_set(inter2, n);
			mpz_add(inter2, inter2, forms[i]);
			test_unique_n(inter2, maxis+i);
			mpz_clear(inter2);
		}
		for(unsigned int i=0; i<forms_len; i++){
			if(mpz_cmp(maxis[i], max_alt) > 0){
				mpz_set(max_alt, maxis[i]);
				mpz_set(n_max, n);
				mpz_add(n_max, n_max, forms[i]);
				gmp_printf("nouvelle altitude maximale : %Zd avec n = %Zd\nen %f\n", max_alt, n_max, (double)(clock()-start)/CLOCKS_PER_SEC);
			}
		}
		mpz_add(maxi, maxi, len_e);
		mpz_add(n, n, len_e);
	}
	#pragma omp parallel for
	for(unsigned long long i=0; i<forms_len; i++)
		mpz_clear(maxis[i]);
	free(maxis);
}

int main(){
	// ne marche pas encore assez rapidement ...
	unsigned long len_forms = 1ull<<24;// 2**24 = 2²⁴ = 16 777 216
	unsigned long long* forms;
	printf("test de toutes les formes de la forme %lu k+p, ça peut prendre du temps...\n", len_forms);
	unsigned long long forms_len = test_all_form_of_nk(len_forms, &forms);
	printf("fin, calcul tous les écarts de la liste des formes\n");
	//unsigned long long* ecarts = calloc(len_forms, sizeof(unsigned long long));
	mpz_t* forms_mpz = calloc(forms_len, sizeof(mpz_t));
	ulls_to_mpzs(forms_mpz, forms, forms_len);
	printf("fin\n");
	parallel(forms_len, forms_mpz, forms[0], len_forms);
	return 0;
}

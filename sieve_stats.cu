#include <inttypes.h>
#include <stdio.h>
int getSPcores(cudaDeviceProp devProp){  
	int cores = 0;
	int mp = devProp.multiProcessorCount;
	switch (devProp.major){
		case 2: // Fermi
			if (devProp.minor == 1) cores = mp * 48;
			else cores = mp * 32;
			break;
		case 3: // Kepler
			cores = mp * 192;
			break;
		case 5: // Maxwell
			cores = mp * 128;
			break;
		case 6: // Pascal
			if ((devProp.minor == 1) || (devProp.minor == 2)) cores = mp * 128;
			else if (devProp.minor == 0) cores = mp * 64;
			else printf("Unknown device type\n");
			break;
		case 7: // Volta and Turing
			if ((devProp.minor == 0) || (devProp.minor == 5)) cores = mp * 64;
			else printf("Unknown device type\n");
			break;
		case 8: // Ampere
			if (devProp.minor == 0) cores = mp * 64;
			else if (devProp.minor == 6) cores = mp * 128;
			else if (devProp.minor == 9) cores = mp * 128; // ada lovelace
			else printf("Unknown device type\n");
			break;
		case 9: // Hopper
			if (devProp.minor == 0) cores = mp * 128;
			else printf("Unknown device type\n");
			break;
		default:
			printf("Unknown device type\n"); 
			break;
	}
	return cores;
}
__device__ __host__ int find_deltaN(int d){
	int deltaN;
	if (d<=5)deltaN = 0;
	else if (d<=18)deltaN = 1;
	else if (d<=24)deltaN = 6;
	else if (d<=27)deltaN = 12;
	else if (d<=29)deltaN = 25;
	else if (d<=32)deltaN = 34;
	else if (d<=33)deltaN = 37;
	else if (d<=35)deltaN = 46;
	else if (d<=37)deltaN = 88;
	else if (d<=40)deltaN = 120;
	else if (d<=43)deltaN = 208;
	else{        // needs experimental reduction
		int minC = (int)(0.6309297535714574371 * d + 1.0); // add 1 to get ceiling
		int minC3 = 1;
		for(int i=minC; i; i--)
			minC3 *= 3;     // 3^minC
		double deltaNtemp = 0.0;
		for(int j=minC; j; j--){
			deltaNtemp = (3.0 * deltaNtemp + 1.0) / 2.0;
		}
		deltaNtemp = deltaNtemp * ((1<<d) - (1<<minC)) / minC3;
		deltaN = (int)(deltaNtemp + 1.0);    // add 1 to get ceiling
	}
	return deltaN;
}
#define SIEVE_COLLATZ_CUDA
static void HandleError( cudaError_t err,
						 const char *file,
						 int line ) {
	if (err != cudaSuccess) {
		printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
				file, line );
		exit( EXIT_FAILURE );
	}
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))
__device__ char source_brut(uint64_t b, uint64_t b0, uint64_t deltaN, int k, int c){
	uint64_t lenList, bm, cm;
	lenList = min(deltaN+1, b0-1);
	for(uint64_t m=1; m<lenList; m++) {    // loop over starting numbers
		bm = b0-m;
		cm = 0;
		// take steps to update lists
		for(int j=k; j; j--) {
			if (bm & 1) {                 // bitwise test for odd
				bm += (bm >> 1)+1;    // note that bm is odd
				cm++;
			} else {
				bm >>= 1;
			}
		}
		// check against original path
		if ( bm==b && cm==c ) {
			return 1;
		}
	}
	return 0;
}
__device__ char test_p1(uint64_t b, uint64_t bd, uint64_t nodd, uint64_t len, uint64_t d){
	uint64_t a=1;
	int ecartp1 = find_deltaN(d+1);
	for(int j=nodd; j; j--)a *= 3;
	uint64_t b1 = b, b2 = b+a;
	int nodd0=nodd, nodd1=nodd;
	if(b1&1){
		b1 += (b1>>1)+1;
		b2 >>= 1;
		nodd0++;
	}else{
		b2 += (b2>>1)+1;
		b1 >>= 1;
		nodd1++;
	}
	if( (b1 >  bd      && !source_brut(b1, bd    , ecartp1, d+1, nodd0))||\
		(b2 > (bd|len) && !source_brut(b2, bd|len, ecartp1, d+1, nodd1))){
		return 1;
	}else{
		return 0;
	}
}
const unsigned char sieve8[16] = {27,  31,  47,  71,  91, 103, 111, 127, 155, 159, 167, 191, 231, 239, 251, 255};
__global__ void find_forms(unsigned long long int* count_normal, unsigned long long int* count_p1, unsigned long long int* count_join, unsigned long long int* count_rest, uint64_t len, uint64_t maximum, unsigned char* sieve8, unsigned char d, uint64_t ecart, uint64_t ecartp1){
	/*uint64_t* LI = (uint64_t*)malloc(sizeof(uint64_t)*d*2);
	unsigned char* L2 = (unsigned char*)malloc(sizeof(unsigned char)*d*2);
	unsigned char* L3 = (unsigned char*)malloc(sizeof(unsigned char)*d*2);*/
	uint64_t n = threadIdx.x+blockIdx.x*blockDim.x;
	uint64_t add = blockDim.x*gridDim.x;
	for(;n<maximum; n += add){
		uint64_t a=1;
		uint64_t b = ((((uint64_t)n)>>4)<<8)^sieve8[n&15];
		uint64_t bd=b;
		unsigned char nodd=0;
		for(int i=d; i; i--){
			if(b&1){
				// *3+1)/2
				a *= 3;
				b += (b>>1)+1;
				nodd++;
			}else{
				b >>= 1;
				if(b<bd)break;
			}
		}
		if(b>bd){
			//if(!source(a, b, ad, bd, neven, d, LI, L2, L3)){
			if(!source_brut(b, bd, ecart, d, nodd)){
				uint64_t b1 = b, b2 = b+a;
				int nodd0=nodd, nodd1=nodd;
				if(b1&1){
					b1 += (b1>>1)+1;
					b2 >>= 1;
					nodd0++;
				}else{
					b2 += (b2>>1)+1;
					b1 >>= 1;
					nodd1++;
				}
				if( (b1 >  bd      && !source_brut(b1, bd    , ecartp1, d+1, nodd0))||\
					(b2 > (bd|len) && !source_brut(b2, bd|len, ecartp1, d+1, nodd1))){
					atomicAdd(count_rest, 1ULL);
				}else{
					atomicAdd(count_p1, 1ULL);
				}
			}else{
				atomicAdd(count_join, 1ULL);
			}
		}else{
			atomicAdd(count_normal, 1ULL);
		}
	}
}
#define lb3 1.5849625007211563 //log2(3)
int main(int argc, char* argv[]){
	cudaDeviceProp devProp;
    cudaGetDeviceProperties(&devProp, 0);
    unsigned int maxThreadPerBlocks = devProp.maxThreadsPerBlock;
	if(argc == 1){
		printf("pas assez d'arguments\n");
		exit(1);
	}
	int power = atoi(argv[1]);
	uint64_t len = ((uint64_t)1)<<power;
	uint64_t maximum = len*16/256;
	uint64_t ecart   = find_deltaN(power);
	uint64_t ecartp1 = find_deltaN(power+1);
	
	unsigned long long int* count_normal;
	unsigned long long int* count_join;
	unsigned long long int* count_p1;
	unsigned long long int* count_rest;
	HANDLE_ERROR(cudaMalloc((void**)&count_normal, sizeof(unsigned long long int)));
	HANDLE_ERROR(cudaMalloc((void**)&count_join  , sizeof(unsigned long long int)));
	HANDLE_ERROR(cudaMalloc((void**)&count_p1    , sizeof(unsigned long long int)));
	HANDLE_ERROR(cudaMalloc((void**)&count_rest  , sizeof(unsigned long long int)));
	
	unsigned char* dev_sieve8;
	HANDLE_ERROR(cudaMalloc((void**)&dev_sieve8, 16));
	HANDLE_ERROR(cudaMemcpy(dev_sieve8, &sieve8, 16, cudaMemcpyHostToDevice));

	find_forms<<<getSPcores(devProp), maxThreadPerBlocks>>>(count_normal, count_p1, count_join, count_rest, len, maximum, dev_sieve8, power, ecart, ecartp1);
	
	uint64_t count[4];
	HANDLE_ERROR(cudaMemcpy(&count[0], count_normal, sizeof(uint64_t), cudaMemcpyDeviceToHost));
	HANDLE_ERROR(cudaMemcpy(&count[1], count_join  , sizeof(uint64_t), cudaMemcpyDeviceToHost));
	HANDLE_ERROR(cudaMemcpy(&count[2], count_p1    , sizeof(uint64_t), cudaMemcpyDeviceToHost));
	HANDLE_ERROR(cudaMemcpy(&count[3], count_rest  , sizeof(uint64_t), cudaMemcpyDeviceToHost));
	printf("exclue nomalement : %llu (%f\%)\nexclue car rejoint un autre : %llu (%f\%)\nexclue en faisant un p1 : %llu (%f\%)\nrestant : %llu (%f\%)\n", count[0], count[0]*100.0/maximum, count[1], count[1]*100.0/maximum, count[2], count[2]*100.0/maximum, count[3], count[3]*100.0/maximum);
}

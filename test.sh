nvcc sieve_stats.cu -O3
for i in {24..39}
do
	echo "     i=$i"
	time ./a.out $i
	echo ""
done
nvcc sieve_stats_u128.cu -O3
for i in {39..79}
do
	echo "     i=$i"
	time ./a.out $i
	echo ""
done

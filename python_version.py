import numba as nb, time

def test_all_form_of_nk(n):
	"""retourne toutes les formes où syr_of_form retourne False où le multiple de k est égal au nombre fourni"""
	L = [None]*n
	for p in range(n):
		if not syr_of_form([n, p]):
			L[p] = 1
	return [p for p, v in enumerate(L) if v]

def syr_of_form(F):
	"""retourne si la forme F passe sous lui-même avec l'application de syracuse :
	>>> syr_of_form([4, 1])
	True
	car 4 k+1 est impaire, donne :
		12k+4 qui est paire donc donne:
		6 k+2 qui est paire donc donne:
		3 k+1 3k+1 < 4k+1, donc le vol en altitude est fini"""
	Fdep = F[:]
	while not F[0]&1 and F[0] >= Fdep[0]:
		if F[1]&1:
			#*3+1
			F[0] *= 3
			F[1] *= 3
			F[1] += 1
		else:
			# //2
			F[0] >>= 1
			F[1] >>= 1
	return F[0] < Fdep[0]

def max_alti_int(debut):
	# on initialise
	max_alt = 0
	n_max = 0
	indice = 0
	mini = 0
	# on transforme mini pour le rendre sous une bonne forme
	# car min est toujours divisible par 65536
	n = mini^forms[0]
	# et on commence
	while (1):#c'est plus rapide
		# n est forcément impair grâce à mon algo
		i = n+(n>>1)^1 # marche seulement pour les 4k+3
		while (1):# c'est toujour plus rapide
			i = i+(i>>1)+1
			if not i&1:
				i2 = i << 1
				if i2 > max_alt:# pour avoir la plus haute altitude
					max_alt, n_max = i2, n
					print(n)
					print("nouvelle altitude maximale :", max_alt, "avec n =", n)
					print("en :", round(time.process_time()-debut, 9), "sec")
				while(1):
					i >>= 1
					if i&1:break
				if i < n:break
		indice += 1
		if indice == forms_len:
			n += ecarts[-1]
			indice = 0
		else:
			n += ecarts[indice-1]

# ne marche pas encore assez rapidement ...
len_forms = 1<<24# 2**24 = 2²⁴
print('test de toutes les formes de la forme ', len_forms, 'k+p, ça peut prendre du temps...', sep='')
forms = test_all_form_of_nk(len_forms)
print('fin, calcul tous les écarts de la liste des formes')
ecarts = [forms[i]-forms[i-1] for i in range(1, len(forms))]
ecarts.append(forms[0]+len_forms-forms[-1])
print('fin')
forms_len = len(forms)
print(forms_len)
if __name__ == '__main__':
	alti = 0 #916613029076867799856
	n_max = 0# 77566362559
	pas = 2**24 # 2**24 = 2²⁴ = 16 777 216
	debut = time.process_time()
	max_alti_int(debut)

#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include <unistd.h>
#include <time.h> 


unsigned long long add(unsigned long long** R, unsigned long long el, unsigned long long len){
	if(len){
		unsigned long long* Ri = calloc(len, sizeof(unsigned long long));
		for(int i=0; i<len; i++)Ri[i] = (*R)[i];
		(*R) = reallocarray((void*)(*R), (size_t)(len+1), sizeof(unsigned long long));
		if(*R == NULL){
			printf("error\n");
			exit(1);
		}
		for(int i=0; i<len; i++)(*R)[i]=Ri[i];
		(*R)[len] = el;
		free(Ri);
		return len+1;
	}else{
		if(((*R)=reallocarray(NULL, 1, sizeof(unsigned long long))) == NULL){
			printf("error\n");
			exit(1);
		}
		(*R)[0] = el;
		return 1;
	}
}

void ull2mpz(mpz_t n, unsigned long long ull)
{
    mpz_set_ui(n, (unsigned int)(ull >> 32)); /* n = (unsigned int)(ull >> 32) */
    mpz_mul_2exp(n, n, 32);                   /* n <<= 32 */
    mpz_add_ui(n, n, (unsigned int)ull);      /* n += (unsigned int)ull */
}

void ulls_to_mpzs(mpz_t* rop, unsigned long long* L, unsigned long long len){
	#pragma omp parallel for
	for(int i=0; i<len; i++){
		mpz_init(rop[i]);
		ull2mpz(rop[i], L[i]);
	}
}

void all_ecarts(unsigned long long* R, unsigned long long* W, unsigned long long lenW, unsigned long long thE){
	#pragma omp parallel for
	for(int i=1; i<lenW; i++){
		R[i-1] = W[i]-W[i-1];
	}
	R[lenW-1] = W[0]+thE-W[lenW-1];
}
char syr_of_form(unsigned long long a, unsigned long long b){
	/*retourne si la forme F passe sous lui-même avec l'application de syracuse :
	>>> syr_of_form([4, 1])
	True
	car 4 k+1 est impaire, donne :
		12k+4 qui est paire donc donne:
		6 k+2 qui est paire donc donne:
		3 k+1 3k+1 < 4k+1, donc le vol en altitude est fini*/
	unsigned long long ad=a, bd=b;
	while((!(a&1)) && a >= ad){
		if(b&1){
			// *3+1
			a *= 3;
			b = b*3+1;
		}
		while(!(b&1 || a&1)){
			// /2
			a >>= 1;
			b >>= 1;
		}
	}
	return a<ad;
}

unsigned int test_all_form_of_nk(unsigned long long n, unsigned long long** L){
	//retourne toutes les formes où syr_of_form retourne False où le multiple de k est égal au nombre fourni
	unsigned int lenL= 0;
	for(unsigned long long p=1; p<n; p+=2){
		if(!syr_of_form(n, p))
			lenL = add(L, p, lenL);
		if(p%10000 == 1)
			printf("%llu %u\n", p, lenL);
	}
	return lenL;
}


void apply(mpz_t i, mpz_t n){
	//gmp_printf("%Zd %Zd\n", i, n);
	mpz_mul_ui(i, n, 3);
	mpz_add_ui(i, i, 1);
	mpz_div_2exp(i, i, 1);
}

void max_alti_int(unsigned long long forms_len, mpz_t* ecarts, unsigned long long first){
	// on initialise
	unsigned int indice = 0;
	clock_t start = clock();
	mpz_t max_alt, n_max, mini;
	mpz_inits(max_alt, n_max, mini, (mpz_ptr)(0));
	// on transforme mini pour le rendre sous une bonne forme
	// car min est toujours divisible par 65536
	mpz_t n, i, i2;
	mpz_init(n);
	mpz_init_set_ui(i, 1);
	mpz_init_set_ui(i2, 1);
	ull2mpz(n, first);
	mpz_t un;
	mpz_init_set_ui(un, 1);
	int t;
	//mpz_t inter;
	//mpz_init(inter);
	// et on commence
	while (1){//c'est plus rapide
		// n est forcément impair grâce à mon algo
		//gmp_printf("n = %Zd\n", n);
		apply(i, n);
		while (1){// c'est toujour plus rapide
			apply(i, i);// syracuse pour les nombres impaires
			mpz_and(i2, i, un);
			if(mpz_cmp_ui(i2, 0) == 0){
				mpz_mul_2exp(i2, i, 1);
				if(mpz_cmp(i2, max_alt)>0){// pour avoir la plus haute altitude
					mpz_set(max_alt, i2);
					mpz_set(n_max, n);
					gmp_printf("nouvelle altitude maximale : %Zd avec n = %Zd\nen %f\n", max_alt, n, (double)(clock()-start)/CLOCKS_PER_SEC);
				}
				mpz_div_2exp(i, i, mpz_scan1(i, 0));
				/*do{
					mpz_div_2exp(i, i, 1);
					mpz_and(i2, i, un);
				}while(mpz_cmp_ui(i2, 0) == 0);*/
				if(mpz_cmp(i, n) < 0)break;
			}
		}
		indice ++;
		if(indice == forms_len){
			mpz_add(n, n, ecarts[indice-1]);
			indice = 0;
		}else
			mpz_add(n, n, ecarts[indice-1]);
			
	}
}
int main(){
	// ne marche pas encore assez rapidement ...
	unsigned long len_forms = 1ull<<24;// 2**24 = 2²⁴ = 16 777 216
	unsigned long long* forms;
	printf("test de toutes les formes de la forme %lu k+p, ça peut prendre du temps...\n", len_forms);
	unsigned long long forms_len = test_all_form_of_nk(len_forms, &forms);
	printf("fin, calcul tous les écarts de la liste des formes\n");
	unsigned long long* ecarts = calloc(len_forms, sizeof(unsigned long long));
	mpz_t* ecarts_mpz = calloc(len_forms, sizeof(mpz_t));
	all_ecarts(ecarts, forms, forms_len, len_forms);
	ulls_to_mpzs(ecarts_mpz, ecarts, len_forms);
	printf("fin\n");
	max_alti_int(forms_len, ecarts_mpz, forms[0]);
	return 0;
}

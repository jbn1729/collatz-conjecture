#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <cuda.h>
static void HandleError( cudaError_t err,
						 const char *file,
						 int line ) {
	if (err != cudaSuccess) {
		printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
				file, line );
		exit( EXIT_FAILURE );
	}
}
#define BILLION  1000000000L;
//https://dev.library.kiwix.org/content/stackoverflow_en_nopic_2021-08/questions/11656241/how-to-print-uint128-t-number-using-gcc
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

#define P10_UINT64 10000000000000000000ULL	 /* 19 zeroes */
#define E10_UINT64 19

#define STRINGIZER(x)	 # x
#define TO_STRING(x)	STRINGIZER(x)

__device__ static int dev_print_u128_u(__uint128_t u128)
{
	int rc;
	if (u128 > UINT64_MAX){
		__uint128_t leading	= u128 / P10_UINT64;
		uint64_t trailing = u128 % P10_UINT64;
		rc = dev_print_u128_u(leading);
		rc += printf("%." TO_STRING(E10_UINT64) PRIu64, trailing);
	}else{
		uint64_t u64 = u128;
		rc = printf("%" PRIu64"\n", u64);
	}
	printf("\n");
	return rc;
}
static int print_u128_u(__uint128_t u128)
{
	int rc;
	if (u128 > UINT64_MAX){
		__uint128_t leading	= u128 / P10_UINT64;
		uint64_t trailing = u128 % P10_UINT64;
		rc = print_u128_u(leading);
		rc += printf("%." TO_STRING(E10_UINT64) PRIu64, trailing);
	}else{
		uint64_t u64 = u128;
		rc = printf("%" PRIu64, u64);
	}
	return rc;
}



/*__device__ inline int ctz_u128 (__uint128_t u) {
	uint64_t hi = u>>64;
	uint64_t lo = (uint64_t)u;
	if(lo){
		return __builtin_ctzll(lo);
	}else if(hi){
		return __builtin_ctzll(hi)+64;
	}else{
		return 128;
	}
}*/

const int sieve8[19] = {27, 31, 47, 63, 71, 91, 103, 111, 127, 155, 159, 167, 191, 207, 223, 231, 239, 251, 255};
__global__ void find_forms(char* L, uint64_t len, uint64_t maximum, uint32_t* c){
	const int sieve8[19] = {27, 31, 47, 63, 71, 91, 103, 111, 127, 155, 159, 167, 191, 207, 223, 231, 239, 251, 255};
	__uint128_t n = threadIdx.x+blockIdx.x*blockDim.x;
	__uint128_t add = blockDim.x*gridDim.x;
	for(;n<maximum; n += add){
		__uint128_t a=len;
		__uint128_t b = (n/19)*256^sieve8[n%19];
		uint64_t ad=len;//, bd=b;
		while((!(a&1)) && a >= ad){
			if(b&1){
				// *3+1
				a *= 3;
				b = b*3+1;
			}
			while(!(b&1 || a&1)){
				// /2
				a >>= 1;
				b >>= 1;
			}
		}
		if(a>ad){
			L[n] = 1;
			atomicAdd(c, 1);
		}else{
			L[n] = 0;
		}
	}
}
__global__ void testnumber(uint64_t mini, uint64_t maxi, uint32_t* forms, __uint128_t* max_alt, uint32_t maximum){
	__uint128_t _n = threadIdx.x+blockIdx.x*blockDim.x;
	__uint128_t add = blockDim.x*gridDim.x;
	__uint128_t i2 = 0;
	for(; _n < maximum; _n += add){
		__uint128_t n = mini^forms[_n];
		if(n%3 == 2)continue;
		__uint128_t i = (n>>1)+n+1;
		while (1){
			while(i&3 == 3){
				i = (i<<1)+(i>>2)+2;
			}
			if(i&1)
				i += (i>>1) + 1;
			if (!(i&1)){
				i2 = i<<1;
				if(i2 > max_alt[_n]){
					max_alt[_n] = i2;
				}
				while(!(i&1))i >>= 1;
				//i >>= ctz_u128(i);
				if(i<n)break;
			}
		}
	}
}

int main(){
	//determine le bon nombre de blocks en fonction du maximum
    cudaDeviceProp devProp;
    cudaGetDeviceProperties(&devProp, 0);
    unsigned int maxThreadPerBlocks = devProp.maxThreadsPerBlock;
	//détermine le bon power en fonction de la mémoire
	size_t free, total;// inittialisation
	CUdevice dev;
	CUcontext ctx;
	cuInit(0);
	cuDeviceGet(&dev, 0);
	cuCtxCreate(&ctx, 0, dev);// fin
	cuMemGetInfo(&free, &total);//informations sur la mémorie libre et total
	size_t len_max = free*90/100;//90/100 pour prendre une marge
	size_t n = len_max;
	int i=0;
	while ((n >>= 1) > 1)i ++;//i = |log2(n)|
	char used64 = 0;
	if(i > 32){
		i--;
		used64 = 1;
	}
	i = (i|1)-1; // le rend paire, car souvent le % gagné entre 2**(2k) et 2**(2k+1) est égale
	char* dev_L;
	uint32_t power = (uint32_t)32;
	printf("%llu\n", power);
	uint64_t len = ((uint64_t)1)<<power;
	uint32_t* dev_c;
	uint32_t c = 0;
	size_t number = len*19/256;
	char* L = (char*)calloc(number, sizeof(char));
	printf("%llu\n", number);
	HANDLE_ERROR(cudaMalloc((void**)&dev_L, number));
	HANDLE_ERROR(cudaMalloc((void**)&dev_c, sizeof(uint32_t)));
	HANDLE_ERROR(cudaMemcpy(dev_c, &c, sizeof(uint32_t), cudaMemcpyHostToDevice));
	find_forms<<<maxThreadPerBlocks, maxThreadPerBlocks>>>(dev_L, len, number, dev_c);
	cudaDeviceSynchronize();
	printf("%s\n", cudaGetErrorString( cudaGetLastError()));
	HANDLE_ERROR(cudaMemcpy(L , dev_L, number			, cudaMemcpyDeviceToHost));
	HANDLE_ERROR(cudaMemcpy(&c, dev_c, sizeof(uint32_t), cudaMemcpyDeviceToHost));
	uint32_t* forms=(uint32_t*)malloc(c*sizeof(uint32_t));
	printf("longueur des formes %lu\n", c);
	printf("%f gagnées\n", (1-(double)c/(double)len)*100);
	uint32_t d=0;
	for(uint32_t i=0; i<number; i++){
		if(L[i]){
			forms[d] = (i/19)*256^sieve8[i%19];
			if(d == c)break;
			d++;
		}
	}
	cudaFree(dev_L);
	cudaFree(dev_c);
	cudaFreeHost(L);
	uint32_t* dev_forms;
	__uint128_t* dev_maxalt;
	__uint128_t* maxalt = (__uint128_t*)calloc(c, sizeof(__uint128_t));
	HANDLE_ERROR(cudaMalloc((void**)&dev_forms , c*sizeof(uint32_t)));
	HANDLE_ERROR(cudaMalloc((void**)&dev_maxalt, c*sizeof(__uint128_t)));
	HANDLE_ERROR(cudaMemcpy(dev_forms, forms, c*sizeof(uint32_t), cudaMemcpyHostToDevice));
	uint64_t mini = 0;
	uint32_t forms_len = c;
	uint64_t maxi = len;
	__uint128_t maxialtall = 0;
	uint32_t X=maxThreadPerBlocks;
	uint32_t Y=(forms_len+X-1)/X;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start);
	float accum=0;
	while(maxi > mini){
		testnumber<<<Y, X>>>(mini, maxi, dev_forms, dev_maxalt, forms_len);
		cudaDeviceSynchronize();
		//printf("2\n");
		HANDLE_ERROR(cudaMemcpy(maxalt, dev_maxalt, forms_len*sizeof(__uint128_t), cudaMemcpyDeviceToHost));
		printf("device %s\n", cudaGetErrorString( cudaGetLastError()));
		/*print_u128_u(maxialtall);
		printf("\n");*/
		//printf("%llu %llu\n", mini, maxi);
		for(uint64_t i=0; i<forms_len; i++){
			if(maxalt[i]>maxialtall){
				maxialtall = maxalt[i];
				print_u128_u(maxialtall);
				printf(" avec n = %llu", forms[i]^mini);
				cudaEventRecord(stop);
				cudaEventSynchronize(stop);
				cudaEventElapsedTime(&accum, start, stop);
				printf(" en %f sec\n", accum/1000);
			}
		}
		mini += len;
		maxi += len;
		//printf("%llu %llu\n", mini, maxi);
	}
	cudaFreeHost(forms);
	cudaFree(dev_forms);
}
